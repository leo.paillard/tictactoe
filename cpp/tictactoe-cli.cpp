#include "Tictactoe.hpp"

using namespace std;
string joueurToString(Joueur joueur){
    if(joueur==JOUEUR_EGALITE)
        return "EGALITE";
    if(joueur==JOUEUR_ROUGE)
        return "JOUEUR 1 (ROUGE)";
    if(joueur==JOUEUR_VERT)
        return "JOUEUR 2 (VERT)";
}

int main() {

    Jeu jeu;
    while(jeu.getVainqueur()==JOUEUR_VIDE){
        std::cout<<"TICTACTOE"<<std::endl;
        std::cout<<jeu<<std::endl;
        std::cout<<"Au joueur " + joueurToString(jeu.getJoueurCourant())<<std::endl;
        std::cout<<"Quelle ligne ?"<<std::endl;
        int colonne,ligne;
        cin >> colonne ;
        std::cout<<"Quelle colonne ?"<<std::endl;
        cin >> ligne ;
        jeu.jouer(colonne,ligne);
    }
    cout<<"Vainqueur :"<<endl;
    cout<<joueurToString(jeu.getVainqueur())<<endl;
    cout<<jeu<<endl;
    return 0;
}
