#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            grille[i][j]=JOUEUR_VIDE;
        }
    }
    joueurCourant=JOUEUR_ROUGE;
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    std::cout<<"TICTACTOE"<<std::endl;
    std::cout<<"_________"<<std::endl;
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            os<<jeu.grille[i][j];
        }
        std::cout<< std::endl;
    }
    return os;
}

Joueur Jeu::getVainqueur() const {

    if(grille[0][0]==grille[0][1] and grille[0][1]==grille[0][2]) return grille[0][0];
    if(grille[1][0]==grille[1][1] and grille[1][1]==grille[1][2]) return grille[1][0];
    if(grille[2][0]==grille[2][1] and grille[2][1]==grille[2][2]) return grille[2][0];
    if(grille[0][0]==grille[1][0] and grille[1][0]==grille[2][0]) return grille[0][0];
    if(grille[0][1]==grille[1][1] and grille[1][1]==grille[2][1]) return grille[0][1];
    if(grille[0][2]==grille[1][2] and grille[1][2]==grille[2][2]) return grille[0][2];
    if(grille[0][0]==grille[1][1] and grille[1][1]==grille[2][2]) return grille[0][0];
    if(grille[0][2]==grille[1][1] and grille[1][1]==grille[2][0]) return grille[0][2];
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            if(grille[i][j]==JOUEUR_VIDE) return JOUEUR_VIDE;
        }
    }
    return JOUEUR_EGALITE;
}

Joueur Jeu::getJoueurCourant() const {
    return joueurCourant;
}

bool Jeu::jouer(int i, int j) {
    if(grille[i][j]==JOUEUR_VIDE){
        this->grille[i][j]=joueurCourant;
        this->changerJoueur();
        return true;
    }
    return false;
}
void Jeu::changerJoueur() {
    if(joueurCourant==JOUEUR_ROUGE)joueurCourant=JOUEUR_VERT;
    else joueurCourant=JOUEUR_ROUGE;
}
